export class Employee {
    $key : string;
    nombre : string;
    resumen : string;
    descripcion : string;
    estado : string;
    latitud : string;
    longitud : string;
    lenguaje : string;
    img : string;
    galeria : string;
    puntuacion: string;
    direccion: string;
    categoria: string;
}
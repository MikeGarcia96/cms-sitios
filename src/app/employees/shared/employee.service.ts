import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'
import { Employee} from './employee.model';
@Injectable()
export class EmployeeService {
  employeeList: AngularFireList<any>;
  selectedEmployee: Employee = new Employee();
  constructor(private firebase :AngularFireDatabase ) { }

  getData(){
    this.employeeList = this.firebase.list('sitios');
    return this.employeeList;
  }

  insertEmployee(employee : Employee)
  {
    this.employeeList.push({
      nombre : employee.nombre,
      resumen : employee.resumen,
      descripcion : employee.descripcion,
      estado : employee.estado,
      latitud : employee.latitud,
      longitud : employee.longitud,
      lenguaje : employee.lenguaje,
      img : employee.img,
      galeria : employee.galeria,
      puntuacion: employee.puntuacion,
      direccion: employee.direccion,
      categoria: employee.categoria,
    });
  }

  updateEmployee(employee : Employee){
    this.employeeList.update(employee.$key,
      {
        nombre : employee.nombre,
        resumen : employee.resumen,
        descripcion : employee.descripcion,
        estado : employee.estado,
        latitud : employee.latitud,
        longitud : employee.longitud,
        lenguaje : employee.lenguaje,
        img : employee.img,
        galeria : employee.galeria,
        puntuacion: employee.puntuacion,
        direccion: employee.direccion,
        categoria: employee.categoria,
      });
  }

  deleteEmployee($key : string){
    this.employeeList.remove($key);
  }

}

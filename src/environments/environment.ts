// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBRhojOvdx0HK0VytwK4vIj1-G7_DFu1qo",
    authDomain: "guia-turistica-f8fad.firebaseapp.com",
    databaseURL: "https://guia-turistica-f8fad.firebaseio.com",
    projectId: "guia-turistica-f8fad",
    storageBucket: "guia-turistica-f8fad.appspot.com",
    messagingSenderId: "974870795496"

  }
};
